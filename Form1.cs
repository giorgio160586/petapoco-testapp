﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PetaPoco_TestApp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            GetPersoneByName();
            GetPersoneByStored();
            InsertUpdateDelete();
        }

        private void GetPersoneByName()
        {
            var DB = new AdventureWorks.AdventureWorksDB();
            String Nome = "Ken";

            PetaPoco.Sql query = new PetaPoco.Sql();
            query.Select("*");
            query.From("Person.Person");
            query.Append("where FirstName = @0", Nome);

            var lPersone = DB.Fetch<AdventureWorks.Person>(query).ToList();
        }

        public void GetPersoneByStored()
        {
            var DB = new AdventureWorks.AdventureWorksDB();
            String Nome = "Ken";

            var lPersone = DB.Fetch<AdventureWorks.Person>(";exec dbo.SearchPerson @0", Nome);
        }

        private void InsertUpdateDelete()
        {
            // test di inserimento
            var DB = new PetaPoco_TestApp.AdventureWorksDB("Giorgio");

            var persone = new AdventureWorks.Persone();
            persone.FirstName = "Ken";
            persone.LastName = "Prova";

            DB.Insert(persone);

            // test di modifica
            persone.FirstName = "Ken2";
            persone.LastName = "Prova2";

            DB.Update(persone);

            // test di cancellazione
            DB.DeleteSoft(persone);
        }
    }
}
