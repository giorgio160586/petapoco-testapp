﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetaPoco_TestApp
{
    public class AdventureWorksDB  : AdventureWorks.AdventureWorksDB
    {
        protected string UtenteCorrente { get; set; }

        public AdventureWorksDB(string Utente) : base()
        {
            UtenteCorrente = Utente;
        }

        public new void Save(object poco)
        {
            if (base.IsNew(poco))
            {
                Insert(poco);

            }
            else
            {
                Update(poco);
            }
        }
        public new void Insert(object poco)
        {
            var pd = PetaPoco.PocoData.ForType(poco.GetType(), DefaultMapper);
            
            if (pd.Columns.ContainsKey("DtCreazione"))
            {
                pd.Columns["DtCreazione"].SetValue(poco, DateTime.Now);
            }
            if (pd.Columns.ContainsKey("UtenteCreazione"))
            {
                pd.Columns["UtenteCreazione"].SetValue(poco, UtenteCorrente);
            }
            if (pd.Columns.ContainsKey("Flag"))
            {
                pd.Columns["Flag"].SetValue(poco, 1);
            }


            base.Insert(poco);
        }

        public new void Update(object poco)
        {
            var pd = PetaPoco.PocoData.ForType(poco.GetType(), DefaultMapper);

            if (pd.Columns.ContainsKey("DtModifica"))
            {
                pd.Columns["DtModifica"].SetValue(poco, DateTime.Now);
            }
            if (pd.Columns.ContainsKey("UtenteModifica"))
            {
                pd.Columns["UtenteModifica"].SetValue(poco, UtenteCorrente);
            }
            if (pd.Columns.ContainsKey("Flag"))
            {
                pd.Columns["Flag"].SetValue(poco, 1);
            }


            base.Update(poco);
        }

        public void DeleteSoft(object poco)
        {
            var pd = PetaPoco.PocoData.ForType(poco.GetType(), DefaultMapper);

            if (pd.Columns.ContainsKey("Flag"))
            {
                pd.Columns["Flag"].SetValue(poco, 0);

                if (pd.Columns.ContainsKey("DtEliminazione"))
                {
                    pd.Columns["DtEliminazione"].SetValue(poco, DateTime.Now);
                }
                if (pd.Columns.ContainsKey("UtenteEliminazione"))
                {
                    pd.Columns["UtenteEliminazione"].SetValue(poco, UtenteCorrente);
                }
            }
            else
            {
                throw new Exception("Cancellazione logica non supportata per questo tipo di entità");
            }

            base.Save(poco);
        }
    }
}
